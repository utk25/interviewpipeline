package com.interview.interviewpipeline.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "Interview_table")
public class Interview {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "interview_id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "stage_id")
    private String stageId;

    @Column(name = "prev_interview_id")
    private Integer prevInterviewId;

    @Column(name = "next_interview_id")
    private Integer nextInterviewId;

}


