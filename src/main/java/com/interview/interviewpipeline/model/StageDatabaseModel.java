package com.interview.interviewpipeline.model;


import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "stage_table")
public class StageDatabaseModel {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "stage_id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "stage_id")
    private List<Interview> interviews;

    @Column(name = "prev_stage_id")
    private Integer prevStageId;

    @Column(name = "next_stage_id")
    private Integer nextStageId;

}

