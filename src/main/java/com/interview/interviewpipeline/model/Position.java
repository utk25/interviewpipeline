package com.interview.interviewpipeline.model;

import lombok.Data;

@Data
public class Position {
    private Integer previousStageId;
    private Integer nextStageId;
}
