package com.interview.interviewpipeline.model;

import lombok.Data;

@Data
public class UpdateStageRequestModel {
    private String newName;
    private Position newPosition;
}

