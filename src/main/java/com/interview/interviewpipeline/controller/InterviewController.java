package com.interview.interviewpipeline.controller;

import com.interview.interviewpipeline.model.Interview;
import com.interview.interviewpipeline.service.InterviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class InterviewController {

    @Autowired
    private InterviewService interviewService;

    @RequestMapping("/interviewPipeline/stages/{stageId}/interviews")
    public List<Interview> getAllInterviews(@PathVariable String stageId) {
        return interviewService.getAllInterviews(stageId);
    }

    @RequestMapping("/interviewPipeline/stages/{stageId}/interviews/{interviewId}")
    public Optional<Interview> getInterview(@PathVariable String interviewId) {
        return interviewService.getInterview(interviewId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/interviewPipeline/stages/{stageId}/interviews")
    public void addInterview(@RequestBody Interview interview) {
        interviewService.addInterview(interview);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/interviewPipeline/stages/{stageId}/interviews/{interviewId}")
    public void updateInterview(@RequestBody Interview interview, @PathVariable String interviewId) {
        interviewService.updateInterview(interviewId, interview);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/interviewPipeline/stages/{stageId}/interviews/{interviewId}")
    public void deleteInterview(@PathVariable String interviewId) {
        interviewService.deleteInterview(interviewId);
    }

}