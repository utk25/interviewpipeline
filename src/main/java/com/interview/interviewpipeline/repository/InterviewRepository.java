package com.interview.interviewpipeline.repository;


import com.interview.interviewpipeline.model.Interview;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface InterviewRepository extends CrudRepository<Interview, String> {

}
