package com.interview.interviewpipeline.repository;

import com.interview.interviewpipeline.model.StageDatabaseModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface StageRepository extends CrudRepository<StageDatabaseModel, Integer> {

     @Query("select distinct sdm " +
             "from StageDatabaseModel sdm " +
             "where sdm.prevStageId = :id")
     StageDatabaseModel findStageDatabaseModelByPrevStageId(@Param("id") int id);

     @Query("select distinct sdm " +
             "from StageDatabaseModel sdm " +
             "where sdm.nextStageId = :id")
     StageDatabaseModel findStageDatabaseModelByNextStageId(@Param("id") int id);

}