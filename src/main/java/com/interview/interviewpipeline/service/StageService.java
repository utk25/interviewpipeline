package com.interview.interviewpipeline.service;


import com.interview.interviewpipeline.model.StageDatabaseModel;
import com.interview.interviewpipeline.repository.StageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class StageService {

    @Autowired
    private StageRepository stageRepository;

    public List<StageDatabaseModel> getAllStages() {
        List<StageDatabaseModel> stageDatabaseModels = new ArrayList<>();
        stageRepository.findAll().forEach(stageDatabaseModels::add);
        return stageDatabaseModels;
    }

    public StageDatabaseModel addStage(StageDatabaseModel stageDatabaseModel) {
        return stageRepository.save(stageDatabaseModel);
    }

    public Optional<StageDatabaseModel> getStage(int id) {
        return stageRepository.findById(id);
    }

    public void updateStage(StageDatabaseModel stageDatabaseModel) {
        stageRepository.save(stageDatabaseModel);
    }


    public StageDatabaseModel findStageDatabaseModelByPrevStageId(int stageId) {
        return stageRepository.findStageDatabaseModelByPrevStageId(stageId);
    }

    public StageDatabaseModel findStageDatabaseModelByNextStageId(int stageId) {
        return stageRepository.findStageDatabaseModelByNextStageId(stageId);
    }

}
