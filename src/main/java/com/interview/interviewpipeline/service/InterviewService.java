package com.interview.interviewpipeline.service;


import com.interview.interviewpipeline.model.Interview;
import com.interview.interviewpipeline.repository.InterviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class InterviewService {

    @Autowired
    private InterviewRepository interviewRepository;

    public List<Interview> getAllInterviews(String stageId) {
        List<Interview> interviews = new ArrayList<>();
        interviewRepository.findAll().forEach(interviews::add);
        return interviews;
    }

    public void addInterview(Interview interview) {
        interviewRepository.save(interview);
    }

    public Optional<Interview> getInterview(String id) {
        return interviewRepository.findById(id);
    }

    public void updateInterview(String id, Interview interview) {
        interviewRepository.save(interview);
    }

    public void deleteInterview(String id) {
        interviewRepository.deleteById(id);
    }
}
