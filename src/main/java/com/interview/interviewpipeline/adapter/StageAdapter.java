package com.interview.interviewpipeline.adapter;

import com.interview.interviewpipeline.model.StageDatabaseModel;
import com.interview.interviewpipeline.model.NewStageRequestModel;
import com.interview.interviewpipeline.model.UpdateStageRequestModel;
import com.interview.interviewpipeline.service.StageService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class StageAdapter {

    @Autowired
    private StageService stageService;

    public StageDatabaseModel convertToDatabaseModel(NewStageRequestModel newStageRequestModel) {
        return convertToDatabaseModel(newStageRequestModel, -1);
    }


    public StageDatabaseModel convertToDatabaseModel(@NonNull NewStageRequestModel newStageRequestModel, int position) {
        if(position == -1) {
            return handleInsertAtTop(newStageRequestModel);
         }
         return null;
    }

    public void changeStageName(UpdateStageRequestModel updateStageRequestModel, Integer stageId) {
        Optional<StageDatabaseModel> maybeStageDatabaseModel = stageService.getStage(stageId);
        if(maybeStageDatabaseModel.isPresent()) {
            StageDatabaseModel stageDatabaseModel = maybeStageDatabaseModel.get();
            stageDatabaseModel.setName(updateStageRequestModel.getNewName());
            stageService.updateStage(stageDatabaseModel);
        }

    }

    public void changeStagePosition(UpdateStageRequestModel updateStageRequestModel, Integer stageId) {

        Optional<StageDatabaseModel> maybeStageDatabaseModel = stageService.getStage(stageId);
        if(!maybeStageDatabaseModel.isPresent()) {
            return;
        }
        StageDatabaseModel stageDatabaseModel = maybeStageDatabaseModel.get();

        // moving to top
        if(updateStageRequestModel.getNewPosition().getPreviousStageId() == null || updateStageRequestModel.getNewPosition().getPreviousStageId() == -1) {
            StageDatabaseModel currentTopStage = stageService.findStageDatabaseModelByPrevStageId(-1);
            currentTopStage.setPrevStageId(stageId);
            stageDatabaseModel.setPrevStageId(-1);
            stageDatabaseModel.setNextStageId(currentTopStage.getId());
            stageService.addStage(stageDatabaseModel);
            stageService.addStage(currentTopStage);
            return;
        }

        //moving at bottom
        if(updateStageRequestModel.getNewPosition().getNextStageId() == null || updateStageRequestModel.getNewPosition().getNextStageId() == -1) {
            StageDatabaseModel currentBottomStage = stageService.findStageDatabaseModelByNextStageId(updateStageRequestModel.getNewPosition().getNextStageId());
            currentBottomStage.setNextStageId(stageId);
            stageDatabaseModel.setNextStageId(-1);
            stageDatabaseModel.setPrevStageId(currentBottomStage.getId());
            stageService.addStage(stageDatabaseModel);
            stageService.addStage(currentBottomStage);
            return;

        }

        // in general move
        StageDatabaseModel currentPrevious = stageService.findStageDatabaseModelByPrevStageId(stageDatabaseModel.getPrevStageId());

        StageDatabaseModel currentNext = stageService.findStageDatabaseModelByNextStageId(stageDatabaseModel.getNextStageId());

        StageDatabaseModel previousStageDatabaseModel = stageService.getStage(updateStageRequestModel.getNewPosition().getPreviousStageId()).get();

        StageDatabaseModel nextStageDatabaseModel = stageService.getStage(updateStageRequestModel.getNewPosition().getNextStageId()).get();

        System.out.println(previousStageDatabaseModel.toString());
        System.out.println(nextStageDatabaseModel.toString());
        currentPrevious.setNextStageId(stageDatabaseModel.getNextStageId());
        if(stageDatabaseModel.getNextStageId() != -1) {

        }

        stageDatabaseModel.setPrevStageId(previousStageDatabaseModel.getId());
        stageDatabaseModel.setNextStageId(nextStageDatabaseModel.getId());

        previousStageDatabaseModel.setNextStageId(stageId);
        nextStageDatabaseModel.setPrevStageId(stageId);

        stageService.addStage(stageDatabaseModel);
        stageService.addStage(previousStageDatabaseModel);
        stageService.addStage(nextStageDatabaseModel);

    }


    private StageDatabaseModel handleInsertAtTop(NewStageRequestModel newStageRequestModel) {
        StageDatabaseModel stageDatabaseModel = new StageDatabaseModel();
        stageDatabaseModel.setName(newStageRequestModel.getStageName());
        stageDatabaseModel.setPrevStageId(-1);
        StageDatabaseModel currentTopStage = stageService.findStageDatabaseModelByPrevStageId(-1);
        if(currentTopStage == null) {
            stageDatabaseModel.setNextStageId(-1);
        } else {
            stageDatabaseModel.setNextStageId(currentTopStage.getId());
        }

        StageDatabaseModel afterPersistStageDatabaseModel = stageService.addStage(stageDatabaseModel);

        if(currentTopStage != null) {
            currentTopStage.setPrevStageId(afterPersistStageDatabaseModel.getId());
            stageService.addStage(currentTopStage);
        }

        return afterPersistStageDatabaseModel;
    }


}
